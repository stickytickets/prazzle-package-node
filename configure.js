var tmp = require('tmp');
var extend = require('util')._extend;
var log = require('winston');

module.exports.get = function(options, cb) {

    var keys = options.fn.envHelper.read('.env.required');

    log.debug('Read required keys:', keys);

    var existing = options.fn.envHelper.read('.env');

		var parameters = {};
    if (options.parameters) {
        for (var i = 0; i < options.parameters.length; i++) {
            var p = options.parameters[i];
            parameters[p.name] = p.default;
        }
    }

    var result = extend(keys, parameters, existing);

		options.env	= result;

    return cb(null, options.env);
}

module.exports.set = function(options, cb) {
    log.debug('Setting Configuration: ', options.env);
    options.fn.envHelper.write('.env', options.env);
    cb();
}

var flattenObject = function(ob) {
    var toReturn = {};

    for (var i in ob) {
        if (!ob.hasOwnProperty(i)) continue;

        if ((typeof ob[i]) == 'object') {
            var flatObject = flattenObject(ob[i]);
            for (var x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;

                toReturn[i + '_' + x] = flatObject[x];
            }
        } else {
            toReturn[i] = ob[i];
        }
    }
    return toReturn;
};
