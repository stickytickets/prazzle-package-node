var path = require('path');
var fs = require('fs');
var extend = require('util')._extend;
var log = require('winston');

exports.package = function(options, cb){
	prepareOptions(options, function(options){
		require('./package.js')(options, cb);
	});
}

exports.configure = {};
exports.configure.get = function(options, cb){

	log.level = options.log_level;

	prepareOptions(options, function(options){
		require('./configure.js').get(options, cb);
	});
}

exports.configure.set = function(options, cb)
{

	log.level = options.log_level;

	prepareOptions(options, function(options){
		require('./configure.js').set(options, cb);
	});
}

exports.install = function(options, cb)
{

	log.level = options.log_level;

	prepareOptions(options, function(options){
		require('./install.js')(options, cb);
	});
}

function packageFilepath(options) {
	return path.join(options.outputDir, packageFilename(options, options));
}

function packageFilename(options) {
	return [options.project, '-', options.version, '.zip'].join('');
}

function packageEnvFilepath(options) {
	return path.join(options.outputDir, [options.project, '-', options.version, '.env'].join(''));
}

function readPackageJson() {
	var projectJsonPath = path.resolve('package.json');

	if (!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;

	return require(projectJsonPath);
}

function gitVersion(options, cb) {
	options.fn.gitVersion(options.packageJson.version, function(error, v) {
		if (error) return cb(error);

		options.version = v;
		options.packageJson.version = options.version;

		log.info('Git Version Calculated: %s', options.version);

		cb(null, options);
	});
}


function prepareOptions(options, cb){

	options.name = options.name || options.project;
	options.packageJson = readPackageJson(),
	options.outputDir = options.outputDir || ['..','dist', options.project].join(path.sep);

	gitVersion(options, function(err, options){

		options.packageFilepath =  packageFilepath (options);
		options.packageFilename =  packageFilename (options);
		options.packageEnvFilepath =  packageEnvFilepath (options);

		cb(options);
	});

	return options;
}
