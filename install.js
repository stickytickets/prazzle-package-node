var exec = require('child_process').exec;
var log = require('winston');
var extend = require('util')._extend;
var config = require('./configure');

module.exports = function(options, cb) {

    //If env settings supplied, write the settings and then return the installer.
    if (options.env) {
        config.set(options, function(er, r) {
            return runInstall(options, cb);
        });
    }
    //if env settings NOT supplied, read the current settings and run than run the installer.
    else {
        config.get(options, function(er, r) {
            return runInstall(options, cb);
        })
    }


}

function runInstall(options, cb) {


    var expander = options.fn.variableExpander.resolver(
        extend({
            name: options.name || options.project,
            environment: options.environment || 'Default',
            PRAZZLE_ENV: options.environment || 'Default'
        }, options.env));

    if (!options.install) {
        log.info('No  Install Script for node app: %s, version: %s', options.project, options.packageJson.version);
        return cb();
    }

    var cmdEnv = expander.toHash();
		var out = "";

    var cmd = exec(options.install, {
        env: cmdEnv
    });
    var hasError;

    cmd.stdout.on('data', (data) => {
        log.verbose(data.toString());
				out += data.toString();
    });

    cmd.stderr.on('error', (data) => {
        hasError = true;
				out += data.toString();
        log.error(data.toString());
    });

    cmd.on('close', (code) => {
        if (hasError || code !== 0) {
					console.log(hasError + ' - ' + code + '\n' + out);
					return cb('An error occured during the "npm run install" command.');
        }
        return cb(null, options);
    });
}
